
module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint:{
			files: ['lib/**/*.js'],
			options: {
				debug: true
			}
		},
		mochaTest:{
			test: {
					src:['tests/test.js'],
					options:{
						reporter: 'spec',

				 }
			}
		}
	});

	//load plugins
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-mocha-test');

	//task
	grunt.registerTask('test', ['jshint','mochaTest']);
};

