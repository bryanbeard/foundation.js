var spawn = require('child_process').spawn;
var platform = require('os').platform();
var cmd = /^win/.test(platform) ? 'bin\\mocha-test.bat' : 'bin/mocha-test.sh';
spawn(cmd, [], { stdio: 'inherit' }).on('exit', function(code) {
	process.exit(code);
});