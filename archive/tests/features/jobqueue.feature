@libraries=jobqueue
Feature: Background Job Queueing

Scenario: Add job to job queue

    Given a new job
    when I add a job to the test queue
    #then that queue should have a job in it with the status of queued

Scenario: Process jobs

    Given a new job
    when I add a job to the test queue
    when I process the test queue
    #then that queue should have a job in it with the status of completed