@libraries=email
Feature: Email extension library

Scenario: Send email synchronously

    Given a valid email extension
    #when a test email is sent with subject "Test email"
    then an email should be verified as sent with subject "Test email"

Scenario: Send bulk email

    Given a valid bulk email extension
    #when a test bulk email is sent with subject "Test bulk email"
    #then an email should be verified as sent with subject "Test bulk email"

