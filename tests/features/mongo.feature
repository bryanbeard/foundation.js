@libraries=mongo
Feature: MongoDB extension library

Scenario: Parsing a connection string

    Given a valid Mongo DB extension library
    when a connection string of "mongodb://dev-dbrt1:27017/instanceName?mongos=true" is parsed
    then the resulting configuration database name should be "instanceName"

