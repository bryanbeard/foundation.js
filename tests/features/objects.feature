@libraries=objects
Feature: Objects extension library

Scenario: Check for empty object

    Given an object with value '{}'
    then the object should be empty

Scenario: Object serialization test

    Given an object with value '{"test":1}'
    when that object is querystring serialized
    then the serialized string should equal 'test=1'

Scenario: Pluck multiple values from collection

    Given a test set of people
    when a sample set of data is plucked with multiple fields
    and the results are serialized
    then the serialized string should equal '[{"firstName":"Thein","qty":5},{"firstName":"Michael","qty":3}]'

