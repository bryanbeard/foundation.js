@libraries=logging
Feature: Logging extension library

Scenario: Logging should save output to a trace file

    Given a valid logger
    when the file at the path "./trace.log" is deleted
    when an info message is logged with the message "Test notification"
    then a file at the path "./trace.log" should not be empty after 500 ms

Scenario: Logging should report on unhandled exceptions

    Given a valid logger
    when the file at the path "./trace.log" is deleted
    #when an unhandled exception is thrown
    #then a file at the path "./trace.log" should not be empty after 500 ms

Scenario: Logging should save logs to a database

    Given a valid logger
    when an info message is logged with the message "Database test notifcation"
    then the logger should report that database logging is enabled

Scenario: Log an information message

    Given a valid logger
    when an info message is logged with the message "Test message"
    then a message should be logged containing the text "Test message"
    then the type of the logged message should be "Info"

Scenario: Log an warning message

    Given a valid logger
    when a warning message is logged with the message "Test message"
    then a message should be logged containing the text "Test message"
    then the type of the logged message should be "Warning"

Scenario: Log an error message

    Given a valid logger
    when an error message is logged with the message "Test message"
    then a message should be logged containing the text "Test message"
    then the type of the logged message should be "Error"

