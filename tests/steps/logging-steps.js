var Yadda = require('yadda'),
    English = Yadda.localisation.English,
    assert = require('assert'),
    dictionary = require('../dictionary'),
    log = require('../../lib/logging'),
    fs = require("fs"),
    wait = require('wait.for'),
    should = require('should');

module.exports = (function() {

    var _log;
    var _mongoConnectionString = 'mongodb://localhost:27017/mean-dev?mongos=true';

    var library = English.library(dictionary)

        .given("a valid logger", function(next) {
            // _log = new log( false, true, '', _mongoConnectionString, '' );
            _log = new log({
                // realtimeToken: 'fe70cb97-624a-4816-8646-a4168a794fe1',
                realtimeToken: '0f873b99-e7eb-4b8b-b889-1dbf434f59b9',
                consoleEnabled: false
            });
            next();
        })

        .when("an info message is logged with the message \"$INPUT\"", function(input, next) {
            // Normally: _log.info( input ); .. however must use below when testing since logging is 100% async
            wait.launchFiber( function() { wait.for( _log.info, input ); });

            // Note - without the below 3 lines LogEntries will not finish transmitting logs in tests
            // .. This is b/c the process terminates too quickly - in an API however it will work
            // setTimeout(function() {
            //     next();
            // }, 1500)
            next();
        })

        .when("a warning message is logged with the message \"$INPUT\"", function(input, next) {
            // Normally: _log.warning( input ); .. however must use below when testing since logging is 100% async
            wait.launchFiber( function() { wait.for( _log.warning, input ); });
            next();
        })

        .when("an error message is logged with the message \"$INPUT\"", function(input, next) {
            // Normally: _log.error( input ); .. however must use below when testing since logging is 100% async
            wait.launchFiber( function() { wait.for( _log.error, input ); });
            next();
        })

        .when("an unhandled exception is thrown", function(next) {
            // var i = 2 / 0;
            // RE: https://github.com/flatiron/winston#handling-uncaught-exceptions-with-winston
            // throw new Error("Fire!");
            next();
        })

        .when("the file at the path \"$INPUT\" is deleted", function(input, next) {
            fs.exists( input, function( exists ) {
                if( exists ) fs.unlinkSync( input );
                next();
            } );
        })

        .then("a message should be logged containing the text \"$INPUT\"", function(input, next) {
            // assert.equal(input, _log);
            next();
        })

        .then("the type of the logged message should be \"$INPUT\"", function(input, next) {
            // assert.equal(input, _log);
            next();
        })

        .then("the logger should report that database logging is enabled", function( next ) {
            _log.isDatabaseLoggingEnabled().should.equal( true );
            next();
        })

        .then("a file at the path \"$INPUT\" should not be empty after $MS ms", function( input, ms, next ) {
            // setTimeout( function() {
            //     var fp = input;
            //     fs.exists( fp, function( exists ) {
            //         var fileSizeInBytes = 0;
            //         if( exists ) {
            //             var stats = fs.statSync( fp )
            //             fileSizeInBytes = stats["size"]
            //         }
            //         fileSizeInBytes.should.be.above( 0 );
            //         next();
            //     } );
            // }, ms );
            next();
        })

    return library;
})();
