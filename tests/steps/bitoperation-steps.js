
var Yadda = require('yadda'),
    English = Yadda.localisation.English,
    assert = require('assert'),
    dictionary = require('../dictionary'),
    bitops = require('../../lib/bitoperations.js');

module.exports = (function() {

    var n, a, ia;

    return English.library(dictionary)

        .given("a numeric value of \"$INPUT\"", function(input, next) {
            n = parseInt( input );
            next();
        })

        .given("a binary array of \"$INPUT\"", function(input, next) {
            a = input;
            next();
        })

        .when("I convert the numeric value to a binary array padded to \"$INPUT\" values", function(input, next) {
            a = bitops.getbinary( n, input );
            next();
        })

        .when("I reverse the array", function( next ) {
            a = bitops.reverse( a );
            next();
        })

        .then("the binary array should equal \"$INPUT\"", function(input, next) {
            assert.equal( a, input );
            next();
        })

        .when("I get an index array from the bit mask", function(next) {
            ia = bitops.getIndexArrayFromBitmask( a );
            next();
        })

        .then("the index array should equal \"$INPUT\"", function( input, next ) {
            assert.equal( ia.join(), input );
            next();
        })

})();