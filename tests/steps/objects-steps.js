var Yadda = require('yadda'),
    English = Yadda.localisation.English,
    assert = require('assert'),
    dictionary = require('../dictionary'),
    objects = require('../../lib/objects'),
    _ = require( '../../lib/lodash' )

module.exports = (function() {

    var _o
    var _serializedData
    var _sampleDataSet
    var _sampleDataSetPlucked

    var library = English.library(dictionary)

        .given("an object with value '$INPUT'", function( input, next ) {
            _o = JSON.parse( input )
            next()
        })

        .then("the object should be empty", function( next ) {
            assert.equal( true, objects.isEmpty( _o ) )
            next()
        })

        .given("a test set of people", function( next ) {
            _sampleDataSet = [{firstName : "Thein", city : "ny", qty : 5},{firstName : "Michael", city : "ny", qty : 3}, {firstName : "Bloom", city : "nj", qty : 10} ]
            next()
        })

        .when("a sample set of data is plucked with multiple fields", function( next ) {
            _sampleDataSetPlucked = _.chain(_sampleDataSet).where({city : "ny"})
                .pluckMany( "firstName", "qty").value()
            next()
        })

        .when("that object is querystring serialized", function(next) {
            _serializedData = objects.querystring( _o );
            next();
        })

        .when("the results are serialized", function( next ) {
            _serializedData = JSON.stringify( _sampleDataSetPlucked )
            next()
        })

        .then("the serialized string should equal '$INPUT'", function( input, next ) {
            assert.equal( input, _serializedData )
            next()
        })

    return library
})()
