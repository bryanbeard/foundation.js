var Yadda = require('yadda'),
    English = Yadda.localisation.English,
    assert = require('assert'),
    dictionary = require('../dictionary'),
    strings = require('../../lib/strings')

module.exports = (function() {

    var _s

    var library = English.library(dictionary)

        .given("a test string value of \"$INPUT\"", function(input, next) {
            _s = input
            next()
        })

        .when("that string is run through a test conversion", function(next) {
            _s = strings.test( _s )
            next()
        })

        .when("that string is run through an upperCaseFirstLetter conversion", function(next) {
            _s = _s.upperCaseFirstLetter()
            next()
        })

        .then("the resulting string would be \"$INPUT\"", function(input, next) {
            assert.equal(input, _s)
            next()
        })

    return library
})()
