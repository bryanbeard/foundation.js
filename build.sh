#!/bin/sh
echo $1
osascript -e '

on run parameters
   tell application "Terminal"

        # -----------------------------------------------------------------------
        #  Clear terminal window
        # -----------------------------------------------------------------------
        activate
        do script with command "cd \"" & parameters & "\"" in window 1
        tell application "System Events" to keystroke "k" using command down

        # -----------------------------------------------------------------------
        #  Run unit tests
        # -----------------------------------------------------------------------
        do script with command "make test-b" in window 1

        # -----------------------------------------------------------------------
        #  Automatically scroll up after compilation errors found
        # -----------------------------------------------------------------------
        # delay 1.5
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116

        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116

        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116
        # tell application "System Events" to tell process "Terminal" to key code 116

    end tell
	tell application id "com.sublimetext.3" to activate
end run

' "$@"