
var config = require( 'config' );

var mongo = exports

// To test: delete require.cache[require.resolve('./lib/mongo')]; (new (require('./lib/mongo'))()).parseConnectionString('mongodb://dev-dbrt1:27017/dbname?mongos=true')
mongo.parseConnectionString = function( s ) {

        // Expecting format:
        // 'mongodb://dev-dbrt1:27017/instanceName?mongos=true'
        if(( s || '' )=== '' ) return '';

        var tokens = s.split('/');
        var hostTokens = tokens[2].split(':');
        var optionsTokens = tokens[3].split('?');

        if(( optionsTokens[0] || '' )=== '' ) return undefined;

        return {
            host: hostTokens[0] || '',
            port: parseInt( hostTokens[1] || '27017' ),
            databaseName: optionsTokens[0] || '',
            options: optionsTokens[1] || ''
        };
    }

mongo.getConnectionString = function( cfg ) {

        // Expecting format:
        // "db": {
        //     "host": "localhost",
        //     "port": 27017,
        //     "name": "Warpdrive"
        // },

        var c = cfg || config

        if( ! c.db ) return ''
            // throw 'Unable to location configuration settings'

        // e.g. 'mongodb://127.0.0.1:27017/dbname'
        return 'mongodb://' + c.db.host + ':' + c.db.port + '/' + c.db.name;
    }

mongo.single = function( callback ) {
        return function( err, doc ) {
            if( err ) return callback.apply( this, arguments );
            if( doc.length == 0 ) return callback.apply( this, [ new Error( 'Unable to locate record within set' ) ] );
            return callback.apply( this, [ null, doc ] );
        };
    }

mongo.multiple = function( callback ) {
        return function( err, doc ) {
            if( err ) return callback.apply( this, arguments );
            return callback.apply( this, [ null, doc ] );
        };
    }

    /**
     * Return a unique identifier with the given `len`.
     *
     *     utils.uid(10);
     *     // => "FDaS435D2z"
     *
     * @param {Number} len
     * @return {String}
     * @api private
     */
mongo.uid = function(len) {
      var buf = []
        , chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        , charlen = chars.length;

      for (var i = 0; i < len; ++i) {
        buf.push(chars[this.randomInt(0, charlen - 1)]);
      }

      return buf.join('');
    }

    /**
     * Retrun a random int, used by `utils.uid()`
     *
     * @param {Number} min
     * @param {Number} max
     * @return {Number}
     * @api private
     */
mongo.randomInt = function( min, max ) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * Applies a Mongoose relational entity to the newly selected value on the target
     */
mongo.link = function( key, value ) {

        var ref = key + 'Key';

        if( ! value ) {
            this[ ref ] = undefined
            this[ key ] = undefined
            return
        }

        // e.g.:
        // a.accountKey = account.accountKey
        // a.account = account._id
        // a.crmSystemKey = account.crmSystemKey
        // a.crmSystem = account.crmSystem._id

        this[ ref ] = value[ ref ]
        this[ key ] = value._id

    }
