
var util = require('util'),
    winston = require('winston'),
    MongoDB = require('winston-mongodb').MongoDB,
    WinstonMail = require('winston-mail').Mail,
    WinstonNewrelic = require('winston-newrelic'),
	Logentries = require('winston-logentries-transport').Logentries;
    mongo = require('./mongo');

var logging = function( options ) {

	var o = options || {
		consoleEnabled : false,
		emailEnabled : false,
		webEndpoint : '',
		dbconfig : '',
		filepath : '',
		emailSettings : '',
		realtimeToken : ''
	}

	var _consoleEnabled = o.consoleEnabled || false
	var _emailEnabled = o.emailEnabled || false
	var _webEndpoint = o.webEndpoint
	var _dbconfig = o.dbconfig
	if( ! _dbconfig ) {
		_dbconfig = mongo.parseConnectionString( mongo.getConnectionString() )
		console.log( 'Logging DB config initialized', _dbconfig )
	}
	var _filepath = o.filepath || 'trace.log'
	if( _filepath == '-' ) _filepath = undefined
	var transports = []

	var _emailSettings = o.emailSettings || {
		level: 'error',
		tls: false,
		ssl: true,
		secure: false,
		silent: false,
		to: 'inbox@seanfuture.com',
		from: 'inbox@seanfuture.com',
		port: 465,
		host: 'smtp.gmail.com',
    	username: 'username',
    	password: 'password',
	};

	if( _consoleEnabled )
		transports[transports.length] = new (winston.transports.Console)({
						// handleExceptions: true,
						colorize: true,
		                timestamp: true });

	if( !! _webEndpoint )
		transports[transports.length] = new (winston.transports.Webhook)(
			_webEndpoint ); // ex: { 'host': 'localhost', 'port': 8080, 'path': '/collectdata', handleExceptions: true })

	if( !! _filepath )
		transports[transports.length] = new (winston.transports.File)({
			filename: _filepath, handleExceptions: true });

	if( _emailEnabled )
		// winston.add(winston.transports.Mail, _emailSettings);
		transports[transports.length] = new (WinstonMail)( _emailSettings );
		// transports[transports.length] = new (winston.transports.Mail)( _emailSettings );

	transports[transports.length] = new (WinstonNewrelic)({
		level: 'info',
	});

	if( !! o.realtimeToken )
		transports[transports.length] = new (Logentries)({
			token: o.realtimeToken,
			secure: true,
            levels: {
                debug: 0,
                info: 1,
                warn: 2,
                error: 3
            },
            printerror: true,
            timestamp: true,
            usequotes: false
		})

	if( !! _dbconfig )
        transports[transports.length] = new(winston.transports.MongoDB)({
			// level: 'debug',
			collection: 'systemlog',
            db: _dbconfig.databaseName,
            host : _dbconfig.host,
            port: _dbconfig.port,
			handleExceptions: true,
			safe: true
        });

	var _logger = new (winston.Logger)({
				exitOnError: false,
				// json: true,
			    transports: transports
			  });

    this.debug = function() {
    	var s = Array.prototype.shift.apply( arguments )
    	var t = arguments.length == 0 ? '' : ' ..'
    	var p = ( arguments.length == 1 && typeof( arguments[0] ) == 'object' ? arguments[0] : arguments )
        _logger.log( 'debug', s + t, p );
    }

    this.info = function() {
    	var s = Array.prototype.shift.apply( arguments )
    	var t = arguments.length == 0 ? '' : ' ..'
    	var p = ( arguments.length == 1 && typeof( arguments[0] ) == 'object' ? arguments[0] : arguments )
        _logger.log( 'info', s + t, p );
    }

    this.warning = function() {
    	var s = Array.prototype.shift.apply( arguments )
    	var t = arguments.length == 0 ? '' : ' ..'
    	var p = ( arguments.length == 1 && typeof( arguments[0] ) == 'object' ? arguments[0] : arguments )
        _logger.log( 'warn', s + t, p );
    }

    this.warn = function() {
    	var s = Array.prototype.shift.apply( arguments )
    	var t = arguments.length == 0 ? '' : ' ..'
    	var p = ( arguments.length == 1 && typeof( arguments[0] ) == 'object' ? arguments[0] : arguments )
        _logger.log( 'warn', s + t, p );
    }

    this.error = function() {
    	var s = Array.prototype.shift.apply( arguments )
    	var t = arguments.length == 0 ? '' : ' ..'
    	var p = ( arguments.length == 1 && typeof( arguments[0] ) == 'object' ? arguments[0] : arguments )
        _logger.log( 'error', s + t, p );
    }

    this.isDatabaseLoggingEnabled = function() {
        return _dbconfig !== undefined;
    }

    this.internalLogger = function() {
        return _logger
    }

}
module.exports = logging;
