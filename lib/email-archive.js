var	qs = require('querystring'),
	sendgrid;

function SendGrid(userId, password){
	sendgrid = require('sendgrid')(userId, password);
}

module.exports = SendGrid;

SendGrid.prototype.SendEmail = function(to, from, subject, body, isHtml, callback) {

 	if(!this.ValidateEmailAddress(to))
 	{
 		throw new Error('Invalid Email: to');
 	}
 	if(!this.ValidateEmailAddress(from))
 	{
 		throw new Error('Invalid Email: from');
 	}

	var email     = new sendgrid.Email({
	  to:       to,
	  from:     from,
	  subject:  subject
	});

	if(isHtml)
		email.html = body;
	else
		email.text = body;

	sendgrid.send(email, function(err, json) {
		if (err)
			throw new Error(err);

		if(json.message === 'error')
			throw new Error(json.errors);

		if(callback) callback();

		});

};

SendGrid.prototype.ValidateEmailAddress = function (email)
{
	//validate email address
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};
